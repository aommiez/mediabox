define({
  "name": "MediaBox web API Documentation",
  "version": "1.0.0",
  "description": "server strore images , video file for share fast ",
  "url": "http://110.164.70.60:3000",
  "header": {
    "title": "Hi ! MediaBox web API",
    "content": "<h3 id=\"server-info\">Server Info</h3>\n<ul>\n<li>base url : <a href=\"http://110.164.70.60\">http://110.164.70.60</a></li>\n<li>base port : 3000</li>\n</ul>\n<h3 id=\"version\">Version</h3>\n<p>1.0.0</p>\n<ul>\n<li>upload muti base64 array</li>\n<li>get images custom size</li>\n<li>delete images</li>\n</ul>\n<h3 id=\"tech\">Tech</h3>\n<p>mediabox uses a number of open source projects to work properly:</p>\n<ul>\n<li>[node.js] - evented I/O for the backend</li>\n<li>[Express] - fast node.js network app framework</li>\n<li>[mongodb] - database</li>\n</ul>\n"
  },
  "footer": {
    "title": "Thanks :)",
    "content": "<h3 id=\"mit-license\">MIT License</h3>\n<p>request function : aommiez@gmail.com</p>\n<p>Copyright (c) 2013-2014 MRG</p>\n<p>Author MRG</p>\n"
  },
  "order": [
    "Images"
  ],
  "generator": {
    "version": "0.9.1",
    "time": "2014-12-18T08:31:53.117Z"
  },
  "apidoc": "0.2.0"
});