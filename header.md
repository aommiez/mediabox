### Server Info
- base url : http://110.164.70.60
- base port : 3000

### Version
1.0.0
- upload muti base64 array
- get images custom size
- delete images

### Tech

mediabox uses a number of open source projects to work properly:
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework
* [mongodb] - database

